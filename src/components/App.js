import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { Link, Redirect, Route, Switch, withRouter } from 'react-router-dom'
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { AppBar, Button, Toolbar, Typography, CssBaseline, IconButton, withStyles } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew'
import LoginForm from './LoginForm'
import TasksList from './TasksList'
import TaskEditDialogContainer from './TaskEditDialogContainer'
import { userLogout } from '../actions/users'

function App({isAuthLoaded, isLoggedIn, onLogout, classes}) {
	return (
		<>
			<div className={classes.root}>
				<CssBaseline />
				<AppBar
					position="absolute"
					className={classes.appBar}
				>
					<Toolbar>
						<Typography
							component="h1"
							variant="h6"
							color="inherit"
							noWrap
							className={classes.title}
						>
							coloc-planner
						</Typography>
						<Route
							exact
							path="/"
							render={() => (
								isAuthLoaded && (
									<>
										<IconButton
											color="inherit"
											aria-label="Add task"
											component={Link}
											to="/new"
										>
											<AddIcon />
										</IconButton>
										{
											isLoggedIn ? (
												<IconButton
													color="inherit"
													aria-label="Log out"
													onClick={onLogout}
												>
													<PowerSettingsNewIcon />
												</IconButton>
											) : (
												<Button
													color="inherit"
													aria-label="Login"
													component={Link}
													to="/login"
												>
													Login
												</Button>
											)
										}
									</>
								)
							)}
						/>
					</Toolbar>
				</AppBar>
				<main className={classes.content}>
					<div className={classes.appBarSpacer} />
					<Switch>
						<Route exact path="/" render={() => (
							isAuthLoaded && (
								isLoggedIn ? (
									<TasksList />
								) : (
									<Redirect to="/login" />
								)
							)
						)} />
						<Route path="/login" render={() => (
							isAuthLoaded && (
								isLoggedIn ? (
									<Redirect to="/" />
								) : (
									<LoginForm />
								)
							)
						)} />
					</Switch>
				</main>
			</div>
			<Switch>
				<Route
					path="/new"
					render={() => (
						<TaskEditDialogContainer isNew />
					)}
				/>
				<Route path="/edit/:id" component={TaskEditDialogContainer} />
			</Switch>
		</>
	)
}

const styles = theme => ({
	root: {
		display: 'flex'
	},
	appBar: {
		zIndex: theme.zIndex.drawer + 1,
		transition: theme.transitions.create(['width', 'margin'], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
	},
	appBarSpacer: theme.mixins.toolbar,
	content: {
		flexGrow: 1,
		paddingTop: theme.spacing.unit * 3,
		height: '100vh',
		overflow: 'auto',
	},
	title: {
		flexGrow: 1,
	}
})

const mapStateToProps = state => {
	const auth = state.firebase.auth
	return {
		isAuthLoaded: isLoaded(auth),
		isLoggedIn: isLoaded(auth) && !isEmpty(auth)
	}
}

const mapDispatchToProps = dispatch => ({
	onLogout: () => dispatch(userLogout())
})

export default compose(
	withRouter,
	firebaseConnect(),
	connect(mapStateToProps, mapDispatchToProps),
	withStyles(styles)
)(App)
