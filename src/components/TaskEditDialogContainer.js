import React, { useState } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { Redirect, withRouter } from 'react-router-dom'
import { firebaseConnect, isLoaded, isEmpty, getVal } from 'react-redux-firebase'
import pick from 'lodash/pick'
import { taskCreate, taskEdit } from '../actions/tasks'
import TaskEditDialog, {defaultProps as tedDefaultProps} from './TaskEditDialog'

function TaskEditDialogContainer({isNew, id, isTaskLoaded, task, isAuthLoaded, isLoggedIn, onTaskCreated, onTaskEdited, history}) {
	const [isDialogOpen, setIsDialogOpen] = useState(true)

	function onDialogExited() {
		history.goBack()
	}

	function onDialogClose(values) {
		if (values) {
			isNew ? onTaskCreated(values) : onTaskEdited(id, values)
		}
		setIsDialogOpen(false)
	}

	return isAuthLoaded && (
		isLoggedIn ? (
			isTaskLoaded &&
				<TaskEditDialog
					title={isNew ? 'Add task' : undefined}
					task={task}
					open={isDialogOpen}
					onClose={onDialogClose}
					TransitionProps={{
						onExited: onDialogExited
					}}
				/>
		) : (
			<Redirect to="/" />
		)
	)
	
}

const mapStateToProps = ({firebase}, {auth, isNew, match}) => {
	const id = match.params.id
	const tasks = getVal(firebase, `data/tasks/${auth.uid}`)
	const isTaskLoaded = isLoaded(tasks)
	let task = null
	if (isNew) {
		task = tedDefaultProps.task
	} else if (isTaskLoaded) {
		task = pick(tasks[id], 'name', 'periodicity')
	}
	return {
		id: isNew ? null : id,
		isTaskLoaded,
		task,
		isAuthLoaded: isLoaded(auth),
		isLoggedIn: isLoaded(auth) && !isEmpty(auth)
	}
}

const mapDispatchToProps = dispatch => ({
	onTaskCreated: data => dispatch(taskCreate(data)),
	onTaskEdited: (id, data) => dispatch(taskEdit(id, data))
})

export default compose(
	withRouter,
	connect(({firebase}) => ({
		auth: getVal(firebase, 'auth')
	})),
	firebaseConnect(({auth}) => [
		`/tasks/${auth.uid}`
	]),
	connect(mapStateToProps, mapDispatchToProps)
)(TaskEditDialogContainer)
