import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { FormControl, FormLabel, withStyles, Typography, TextField } from '@material-ui/core'

const timeUnits = [
	'days',
	'weeks'
]

function PeriodicityChooser({periodicity, onChange, classes}) {
	const [count, setCount] = useState(periodicity.count)
	const [timeUnit, setTimeUnit] = useState(periodicity.timeUnit)

	function handleCountChange(e) {
		const value = Number.parseInt(e.target.value, 10)
		setCount(value)
		onChange && onChange({count: value, timeUnit})
	}

	function handleTimeUnitChange(e) {
		const value = e.target.value
		setTimeUnit(value)
		onChange && onChange({count, timeUnit: value})
	}

	return (
		<FormControl component="fieldset" className={classes.fieldset}>
			<FormLabel component="legend">Periodicity</FormLabel>

			<div className={classes.formContainer}>
				<Typography
					variant="body1"
					className={classes.text}
				>
          every
				</Typography>
				<TextField
					name="count"
					type="number"
					value={count}
					className={classes.count}
					onChange={handleCountChange}
				/>
				<TextField
					name="timeUnit"
					select
					value={timeUnit}
					className={classes.timeUnit}
					SelectProps={{
						native: true
					}}
					onChange={handleTimeUnitChange}
				>
					{timeUnits.map(tu => (
						<option key={tu} value={tu}>
							{tu}
						</option>
					))}
				</TextField>
			</div>
		</FormControl>
	)
}

const styles = theme => ({
	count: {
		marginRight: theme.spacing.unit
	},
	fieldset: {
		margin: theme.spacing.unit * 3
	},
	formContainer: {
		display: 'flex',
		alignItems: 'end'
	},
	text: {
		marginRight: theme.spacing.unit
	},
	timeUnit: {
		marginRight: theme.spacing.unit
	}
})

export const propTypes = {
	periodicity: PropTypes.exact({
		count: PropTypes.number.isRequired,
		timeUnit: PropTypes.string.isRequired
	}),
	onChange: PropTypes.func.isRequired
}
export const defaultProps = {
	periodicity: {
		count: 0,
		timeUnit: 'days'
	}
}

PeriodicityChooser.propTypes = propTypes
PeriodicityChooser.defaultProps = defaultProps

export default withStyles(styles)(PeriodicityChooser)
