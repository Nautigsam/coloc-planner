import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { CircularProgress, IconButton, ListItem, ListItemIcon, ListItemText, ListItemSecondaryAction, Menu, MenuItem, withStyles } from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete'
import DoneIcon from '@material-ui/icons/Done'
import EditIcon from '@material-ui/icons/Edit'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import ErrorIcon from '@material-ui/icons/Error'
import red from '@material-ui/core/colors/red'

const LIST_ITEM_ICON_SIZE = '40px'

function Task({task, onTaskDeleted, onTaskDone, classes}) {
	const [menuAnchorEl, setMenuAnchorEl] = useState(null)
	const isMenuOpen = Boolean(menuAnchorEl)

	function onTaskMenuClick(event) {
		setMenuAnchorEl(event.currentTarget)
	}

	function onTaskMenuClose() {
		setMenuAnchorEl(null)
	}

	function onDeleteMenuItemClick() {
		onTaskMenuClose()
		onTaskDeleted(task.id)
	}

	function onTaskDoneClick() {
		onTaskMenuClose()
		onTaskDone(task.id)
	}

	return (
		<ListItem
			divider={true}
			alignItems='flex-start'
		>
			{
				task.completion === 100 ? (
					<ListItemIcon className={classes.listItemCompletedIconWrapper}>
						<ErrorIcon className={classes.listItemCompletedIcon} />
					</ListItemIcon>
				) : (
					<CircularProgress
						size={LIST_ITEM_ICON_SIZE}
						variant="static"
						value={task.completion}
					/>
				)
			}
			<ListItemText
				primary={task.name}
				secondary={task.dueDateText}
			/>
			<ListItemSecondaryAction>
				<IconButton
					aria-label="Task done"
					onClick={onTaskDoneClick}
				>
					<DoneIcon />
				</IconButton>
				<IconButton
					aria-label="More tasks options"
					aria-owns={isMenuOpen ? `task-more-menu-${task.id}` : undefined}
					aria-haspopup="true"
					onClick={onTaskMenuClick}
				>
					<MoreVertIcon />
				</IconButton>
				<Menu
					id={`task-more-menu-${task.id}`}
					anchorEl={menuAnchorEl}
					open={isMenuOpen}
					onClose={onTaskMenuClose}
				>
					<MenuItem
						key="edit"
						component={Link}
						to={`/edit/${task.id}`}
						onClick={onTaskMenuClose}
					>
						<EditIcon /> Edit
					</MenuItem>
					<MenuItem
						key="delete"
						onClick={onDeleteMenuItemClick}
					>
						<DeleteIcon /> Delete
					</MenuItem>
				</Menu>
			</ListItemSecondaryAction>
		</ListItem>
	)
}

const styles = () => ({
	listItemCompletedIcon: {
		fontSize: LIST_ITEM_ICON_SIZE,
		color: red[500]
	},
	listItemCompletedIconWrapper: {
		marginRight: 0
	}
})

export const propTypes = {
	task: PropTypes.shape({
		id: PropTypes.string.isRequired,
		name: PropTypes.string.isRequired,
		completion: PropTypes.number.isRequired,
		nextDueDate: PropTypes.object.isRequired
	}).isRequired,
	onTaskDeleted: PropTypes.func,
	onTaskDone: PropTypes.func
}
Task.propTypes = propTypes

export const defaultProps = {
	onTaskDeleted: () => {},
	onTaskDone: () => {}
}
Task.defaultProps = defaultProps

export default withStyles(styles)(Task)
