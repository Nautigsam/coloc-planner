import React, { useState } from 'react'
import { connect } from 'react-redux'
import { Typography, TextField, Button } from '@material-ui/core'
import { omit } from 'lodash'
import { userLogin } from '../actions/users'

function LoginForm({onLogin}) {
	const [data, setData] = useState({})
	const [errors, setErrors] = useState({})

	function onEmailChange(e) {
		setErrors(omit(errors, 'email'))
		setData(Object.assign({}, data, {email: e.target.value}))
	}
	function onEmailBlur(e) {
		if (!e.target.value) {
			setErrors(Object.assign({}, errors, {email: 'Required'}))
		}
	}

	function onPasswordChange(e) {
		setErrors(omit(errors, 'password'))
		setData(Object.assign({}, data, {password: e.target.value}))
	}
	function onPasswordBlur(e) {
		if (!e.target.value) {
			setErrors(Object.assign({}, errors, {password: 'Required'}))
		}
	}

	function onLoginFormSubmit(e) {
		e.preventDefault()
		onLogin(data)
	}

	return (
		<>
			<Typography variant="h5" gutterBottom component="h2" align="center">
				Log in
			</Typography>
			<form
				id="login-form"
				onSubmit={onLoginFormSubmit}
			>
				<TextField
					type="email"
					label="E-mail"
					name="email"
					error={Boolean(errors.email)}
					onChange={onEmailChange}
					onBlur={onEmailBlur}
					required={true}
				/>
				<br/>
				<TextField
					type="password"
					label="Password"
					name="password"
					error={Boolean(errors.password)}
					onChange={onPasswordChange}
					onBlur={onPasswordBlur}
					required={true}
				/>
				<br/>
				<Button type="submit">Login</Button>
			</form>
		</>
	)
}

const mapDispatchToProps = dispatch => ({
	onLogin: data => dispatch(userLogin(data))
})

export default connect(null, mapDispatchToProps)(LoginForm)
