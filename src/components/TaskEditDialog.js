import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { Dialog, DialogContent, Button, Toolbar, IconButton, AppBar, Typography, Slide, DialogTitle, DialogActions, TextField } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import CloseIcon from '@material-ui/icons/Close'
import withMobileDialog from '@material-ui/core/withMobileDialog'
import omit from 'lodash/omit'
import isEqual from 'lodash/isEqual'
import PeriodicityChooser, {propTypes as pcPropTypes, defaultProps as pcDefaultProps} from './PeriodicityChooser'

function Transition(props) {
	return <Slide direction="up" {...props} />
}

function TaskEditDialog({task, title, open, onClose, fullScreen, classes, ...others}) {
	const [data, setData] = useState(task)
	const [errors, setErrors] = useState({})
	useEffect(() => {
		setData(task)
		setErrors({})
	}, [open])

	function onNameChange(e) {
		setErrors(omit(errors, 'name'))
		setData(Object.assign({}, data, {name: e.target.value}))
	}
	function onNameBlur(e) {
		if (open && !e.target.value) {
			setErrors(Object.assign({}, errors, {name: 'Required'}))
		}
	}

	function onPeriodicityChange(periodicity) {
		setData(Object.assign({}, data, {periodicity}))
	}

	function onFieldKeyDown(e) {
		if (e.key === 'Enter') {
			e.preventDefault()
		}
	}

	function onCancel() {
		onClose()
	}

	function onSave() {
		if (
			Object.keys(errors).length === 0 &&
			isEqual(Object.keys(data), Object.keys(task))
		) {
			onClose(data)
		}
	}

	return (
		<Dialog
			fullScreen={fullScreen}
			open={open}
			onClose={onCancel}
			TransitionComponent={Transition}
			aria-labelledby={fullScreen ? undefined : `edit-task-dialog-title-${task.id}`}
			{...others}
		>
			{fullScreen ?
				<AppBar className={classes.appBar}>
					<Toolbar>
						<IconButton color="inherit" onClick={onCancel} aria-label="Close">
							<CloseIcon />
						</IconButton>
						<Typography variant="h6" color="inherit" className={classes.flex}>
							{title}
						</Typography>
						<Button color="inherit" onClick={onSave}>
							save
						</Button>
					</Toolbar>
				</AppBar>
				:
				<DialogTitle id={`edit-task-dialog-title-${task.id}`}>Edit task</DialogTitle>
			}
			<DialogContent className={classes.content}>
				<form
					id={`edit-task-form-${task.id}`}
					onSubmit={onSave}
				>
					<TextField
						type="text"
						label="Name"
						name="name"
						defaultValue={data.name}
						error={Boolean(errors.name)}
						helperText={errors.name}
						onChange={onNameChange}
						onBlur={onNameBlur}
						onKeyDown={onFieldKeyDown}
						required={true}
					/>
					<br/>
					<PeriodicityChooser
						periodicity={task.periodicity}
						onChange={onPeriodicityChange}
					/>
				</form>
			</DialogContent>
			{!fullScreen &&
				<DialogActions>
					<Button onClick={onCancel} color="primary">
						Cancel
					</Button>
					<Button onClick={onSave} color="primary" autoFocus>
						Save
					</Button>
				</DialogActions>
			}
		</Dialog>
	)
}

const styles = theme => ({
	appBar: {
		position: 'relative'
	},
	content: {
		padding: theme.spacing.unit * 3
	},
	flex: {
		flex: 1
	}
})

export const propTypes = {
	task: PropTypes.exact({
		name: PropTypes.string.isRequired,
		periodicity: pcPropTypes.periodicity
	}).isRequired,
	title: PropTypes.string,
	open: PropTypes.bool.isRequired,
	onClose: PropTypes.func.isRequired,
	fullScreen: PropTypes.bool.isRequired
}
TaskEditDialog.propTypes = propTypes

export const defaultProps = {
	task: {
		name: '',
		periodicity: pcDefaultProps.periodicity
	},
	title: 'Edit task'
}
TaskEditDialog.defaultProps = defaultProps

export default withMobileDialog()(withStyles(styles)(TaskEditDialog))
