import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, getVal } from 'react-redux-firebase'
import { List, Typography, Divider } from '@material-ui/core'
import TaskContainer from './TaskContainer'
import { getTasksIds } from '../selectors/tasks'

function TasksList({isTasksLoaded, taskIds}) {
	return (
		<>
			<Typography variant="h5" gutterBottom component="h2" align="center">
				Tasks
			</Typography>
			<Divider/>
			{isTasksLoaded ? (
				<List>
					{taskIds.map(id => (
						<TaskContainer
							key={id}
							id={id}
						/>
					))}
				</List>) : null
			}
		</>
	)
}

const mapStateToProps = ({firebase}, {auth}) => {
	const tasks = getVal(firebase, `data/tasks/${auth.uid}`)
	const isTasksLoaded = isLoaded(tasks)
	return {
		isTasksLoaded,
		taskIds: isTasksLoaded ? getTasksIds(tasks) : null
	}
}

export default compose(
	connect(({firebase}) => ({
		auth: getVal(firebase, 'auth')
	})),
	firebaseConnect(({auth}) => [
		`/tasks/${auth.uid}`
	]),
	connect(mapStateToProps)
)(TasksList)
