import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect, getVal } from 'react-redux-firebase'
import PropTypes from 'prop-types'
import { distanceInWords, isBefore, startOfDay } from 'date-fns'
import { taskDelete, taskDone} from '../actions/tasks'
import { getCompletion, getNextDueDate } from '../selectors/task'
import Task, {propTypes as taskPropTypes} from './Task'

function TaskContainer({task, onTaskDeleted, onTaskDone}) {
	return (
		<Task
			task={task}
			onTaskDeleted={onTaskDeleted}
			onTaskDone={onTaskDone}
		/>
	)
}

export const propTypes = {
	id: PropTypes.string.isRequired,
	task: taskPropTypes.task,
	onTaskDeleted: PropTypes.func.isRequired,
	onTaskDone: PropTypes.func.isRequired
}
TaskContainer.propTypes = propTypes

const mapStateToProps = ({firebase}, {id}) => {
	const auth = getVal(firebase, 'auth')
	const task = getVal(firebase, `data/tasks/${auth.uid}/${id}`)
	const completion = getCompletion(task)
	const nextDueDate = getNextDueDate(task)
	const now = new Date()
	const distance = distanceInWords(
		startOfDay(now),
		startOfDay(nextDueDate),
		{addSuffix: true}
	)
	let dueDateText
	if (isBefore(nextDueDate, now)) {
		dueDateText = `should have been done ${distance}`
	} else {
		dueDateText = `should be done ${distance}`
	}
	return {
		task: {id, ...task, completion, nextDueDate, dueDateText}
	}
}

const mapDispatchToProps = dispatch => ({
	onTaskDeleted: id => dispatch(taskDelete(id)),
	onTaskDone: id => dispatch(taskDone(id))
})

export default compose(
	firebaseConnect(),
	connect(mapStateToProps, mapDispatchToProps)
)(TaskContainer)
