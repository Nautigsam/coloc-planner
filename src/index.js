import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { applyMiddleware, createStore, compose } from 'redux'
import { logger } from 'redux-logger'
import thunk from 'redux-thunk'
import { BrowserRouter } from 'react-router-dom'
import { reactReduxFirebase, getFirebase } from 'react-redux-firebase'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import './index.css'
import App from './components/App'
import rootReducer from './reducers'
import * as serviceWorker from './serviceWorker'

const fbConfig = {
	apiKey: 'AIzaSyA_-wBP52YtdwE6w1D0JYyVR03Ap2exhFM',
	authDomain: 'coloc-planner-beta.firebaseapp.com',
	databaseURL: 'https://coloc-planner-beta.firebaseio.com',
	projectId: 'coloc-planner-beta',
	storageBucket: 'coloc-planner-beta.appspot.com',
	messagingSenderId: '108686950252'
}
/*if (process.env.NODE_ENV === 'development') {
	fbConfig.databaseURL = 'https://localhost:9000'
}*/
firebase.initializeApp(fbConfig)

const middlewares = [thunk.withExtraArgument(getFirebase)]

if (process.env.NODE_ENV !== 'production') {
	middlewares.push(logger)
}

const config = {
	userProfile: 'users'
}

const store = createStore(
	rootReducer,
	{},
	compose(
		applyMiddleware(...middlewares),
		reactReduxFirebase(firebase, config)
	)
)

ReactDOM.render(
	<BrowserRouter>
		<Provider store={store}>
			<App />
		</Provider>
	</BrowserRouter>,
	document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister()
