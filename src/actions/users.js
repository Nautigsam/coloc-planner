export const USER_LOGIN = 'USER_LOGIN'
export const USER_LOGIN_ERROR = 'USER_LOGIN_ERROR'
export const USER_LOGOUT = 'USER_LOGOUT'
export const USER_LOGOUT_ERROR = 'USER_LOGOUT_ERROR'

export function userLogin(data) {
	return (dispatch, getState, getFirebase) => {
		const firebase = getFirebase()
		firebase
			.login(data)
			.then(
				() => dispatch({type: USER_LOGIN, email: data.email}),
				error => dispatch({type: USER_LOGIN_ERROR, email: data.email, error})
			)
	}
}

export function userLogout() {
	return (dispatch, getState, getFirebase) => {
		const firebase = getFirebase()
		firebase
			.logout()
			.then(
				() => dispatch({type: USER_LOGOUT}),
				error => dispatch({type: USER_LOGOUT_ERROR, error})
			)
	}
}
