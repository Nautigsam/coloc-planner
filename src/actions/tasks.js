export const TASK_CREATE = 'TASK_CREATE'
export const TASK_CREATE_ERROR = 'TASK_CREATE_ERROR'
export const TASK_DELETE = 'TASK_DELETE'
export const TASK_DELETE_ERROR = 'TASK_DELETE_ERROR'
export const TASK_DONE = 'TASK_DONE'
export const TASK_DONE_ERROR = 'TASK_DONE_ERROR'
export const TASK_EDIT = 'TASK_EDIT'
export const TASK_EDIT_ERROR = 'TASK_EDIT_ERROR'

export function taskCreate(data) {
	return (dispatch, getState, getFirebase) => {
		const {firebase: {auth}} = getState()
		const newData = {...data, lastCompletionDate: (new Date()).toISOString()}
		const firebase = getFirebase()
		firebase
			.push(`tasks/${auth.uid}`, newData)
			.then(
				() => dispatch({type: TASK_CREATE, newData}),
				error => dispatch({type: TASK_CREATE_ERROR, newData, error})
			)
	}
}

export function taskDelete(id) {
	return (dispatch, getState, getFirebase) => {
		const {firebase: {auth}} = getState()
		const firebase = getFirebase()
		firebase
			.remove(`tasks/${auth.uid}/${id}`)
			.then(
				() => dispatch({type: TASK_DELETE, id}),
				error => dispatch({type: TASK_DELETE_ERROR, id, error})
			)
	}
}

export function taskDone(id) {
	return (dispatch, getState, getFirebase) => {
		const {firebase: {auth}} = getState()
		const firebase = getFirebase()
		firebase
			.update(`tasks/${auth.uid}/${id}`, {lastCompletionDate: (new Date()).toISOString()})
			.then(
				() => dispatch({type: TASK_DONE, id}),
				error => dispatch({type: TASK_DONE_ERROR, id, error})
			)
	}
}

export function taskEdit(id, data) {
	return (dispatch, getState, getFirebase) => {
		const {firebase: {auth}} = getState()
		const firebase = getFirebase()
		firebase
			.update(`tasks/${auth.uid}/${id}`, data)
			.then(
				() => dispatch({type: TASK_EDIT, id, data}),
				error => dispatch({type: TASK_EDIT_ERROR, id, data, error})
			)
	}
}
