import { createSelector } from 'reselect'
import { parse, addDays, addWeeks, differenceInMinutes } from 'date-fns'

function addPeriodicity(date, periodicity) {
	switch(periodicity.timeUnit) {
	case 'days':
		return addDays(date, periodicity.count)
	case 'weeks':
		return addWeeks(date, periodicity.count)
	default:
		return date
	}
}

const getPeriodicity = state => state.periodicity
const getLastCompletionDate = state => state.lastCompletionDate

export const getNextDueDate = createSelector(
	[getPeriodicity, getLastCompletionDate],
	(periodicity, lastCompletionDate) => addPeriodicity(
		parse(lastCompletionDate),
		periodicity
	)
)

export const getCompletion = createSelector(
	[getPeriodicity, getNextDueDate],
	(periodicity, nextDueDate) => {
		const periodicityInMinutes = differenceInMinutes(
			addPeriodicity(parse(0), periodicity),
			0
		)
		const nextDueDateToNow = differenceInMinutes(new Date(), nextDueDate)
		if (nextDueDateToNow < 0) { // now < nextDueDate
			return Math.floor((periodicityInMinutes + nextDueDateToNow) * 100 / periodicityInMinutes)
		} else { // now >= nextDueDate
			return 100
		}
	}
)
