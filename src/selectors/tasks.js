import { createSelector } from 'reselect'
import { getCompletion } from './task'

const getTasks = state => state || {}

export const getTasksIds = createSelector(
	[getTasks],
	tasks => Object.entries(tasks)
		.map(([id, task]) => [id, {...task, completion: getCompletion(task)}])
		.sort(([, task1], [, task2]) => task2.completion - task1.completion)
		.reduce((acc, [id]) => [...acc, id], [])
)
