const firebase = require('@firebase/testing')
const fs = require('fs')

/*
 * ============
 *    Setup
 * ============
 */
const databaseName = 'coloc-planner-test'
const coverageUrl = `http://localhost:9000/.inspect/coverage?ns=${databaseName}`

const rules = fs.readFileSync('database.rules.json', 'utf8')

/**
 * Creates a new app with authentication data matching the input.
 *
 * @param {object} auth the object to use for authentication (typically {uid: some-uid})
 * @return {object} the app.
 */
function authedApp(auth) {
	return firebase.initializeTestApp({ databaseName, auth }).database()
}

/**
 * Creates a new admin app.
 *
 * @return {object} the app.
 */
function adminApp() {
	return firebase.initializeAdminApp({ databaseName }).database()
}

/*
 * ============
 *  Test Cases
 * ============
 */
before(async () => {
	// Set database rules before running these tests
	await firebase.loadDatabaseRules({
		databaseName,
		rules: rules
	})
})

beforeEach(async () => {
	// Clear the database between tests
	await adminApp()
		.ref()
		.set(null)
})

after(async () => {
	// Close any open apps
	await Promise.all(firebase.apps().map(app => app.delete()))
	console.log(`View rule coverage information at ${coverageUrl}\n`)
})

describe('tasks rules', () => {
	
	it('should allow user to fetch its tasks', async () => {
		const toto = authedApp({ uid: 'toto' })
		const noone = authedApp(null)

		await adminApp()
			.ref('tasks/toto/task1')
			.set({
				name: 'task'
			})

		await firebase.assertSucceeds(
			toto
				.ref('tasks/toto')
				.once('value')
		)
		await firebase.assertFails(
			noone
				.ref('tasks')
				.once('value')
		)
	})

	it('should allow an authed user to create a task', async () => {
		const toto = authedApp({uid: 'toto'})
		const tata = authedApp({uid: 'tata'})
		const noone = authedApp(null)

		await adminApp()
			.ref('tasks/toto/task1')
			.set({
				name: 'Task1'
			})

		await firebase.assertSucceeds(
			toto
				.ref('tasks/toto')
				.push({name: 'task'})
		)
		await firebase.assertFails(
			tata
				.ref('tasks/toto')
				.push({name: 'task'})
		)
		await firebase.assertFails(
			noone
				.ref('tasks/toto')
				.push({name: 'task'})
		)
	})

	it('should allow an authed user to edit one of their tasks', async () => {
		const toto = authedApp({uid: 'toto'})
		const tata = authedApp({uid: 'tata'})
		const noone = authedApp(null)

		await adminApp()
			.ref('tasks/toto/task1')
			.set({
				name: 'task'
			})
		
		await firebase.assertSucceeds(
			toto
				.ref('tasks/toto/task1')
				.update({
					name: 'task-new'
				})
		)
		await firebase.assertFails(
			tata
				.ref('tasks/toto/task1')
				.update({
					name: 'task-new'
				})
		)
		await firebase.assertFails(
			noone
				.ref('tasks/toto/task1')
				.update({
					name: 'task-new'
				})
		)
	})

	it('should allow an authed user to delete one of their tasks', async () => {
		const toto = authedApp({uid: 'toto'})
		const tata = authedApp({uid: 'tata'})
		const noone = authedApp(null)

		await adminApp()
			.ref('tasks/toto/task1')
			.set({
				name: 'task'
			})
		
		await firebase.assertSucceeds(
			toto
				.ref('tasks/toto/task1')
				.remove()
		)
		await firebase.assertFails(
			tata
				.ref('tasks/toto/task1')
				.remove()
		)
		await firebase.assertFails(
			noone
				.ref('tasks/toto/task1')
				.remove()
		)
	})
})
